# AOS 573 Final Project
Harrison Tran

> **Guiding Question**: Does the collection of qualitative maritime observations taken by human-observers resemble coterminous quantitative satellite-derived precipitation estimates?

## About this Project
This final project for AOS 573 extends the research project I have been parttaking in as a research M.S. student with Dr. Grant Petty. However, all of the analyses, visualizations, and workflows conducted as part of this project have not been done before as part of my research, and were performed independently.

Most of the world is covered by ocean, which until the last decades had very sparse precipitation data. Satellite-based precipitation estimates have greatly improved our understanding of the hydrologic cycle, which has vast implications for climate. However, ships existed long before satellites did, which begs the question: can weather reports from ships (largely qualitative) be used to infer qualitative or spatial information about maritime precipitation before the advent of satellites?

The analysis presented in this project, self-contained in `analysis.ipynb`, seeks to work towards answering that question. Quantiative satellite-based rainfall estimates from the [Integrated Multi-satellitE Retrievals for GPM (IMERG)](https://gpm.nasa.gov/data/imerg) and qualitative ship observations from the [International Comprehensive Ocean-Atmosphere Data Set (ICOADS)](https://icoads.noaa.gov) are examined for the year *2010*, to the distribution of rainfall as inferred by these two disparate data sources, and to compare one with another to find out whether or not qualitative ship observations provide any use for inferring actual precipitation patterns. Numerous visualizations are produced by the analysis notebook, which will hopefully be used in the future to analyze other years.

> **NOTE**: Information on the data sources, where they were found, and how they were ordered can be found in `analysis.ipynb`.

## In this repository
This repository consists of the main Jupyter notebook where the analysis and data visualization is performed, the supporting data used by that notebook, and supplemental repository files.

- `IMERG/` is a folder containing netCDF files representing monthly precipitation estimates from the IMERG product for each month in 2010.
- `analysis.ipynb` is the principal file in the repository and contains analysis of the ICOADS and IMERG data and visualizations of the datasets, interpretations of the data and how the analysis is performed, and notes on how the data was processed
- `environment.yml` is the exported environment of the Python environment in which the project was developed (the AOS 573 environment was used)
- `icoads2010.csv` is a pre-processed listing of all crewed weather observations from ships in 2010, and originates from the ICOADS dataset.
- `IMERG_land_sea_mask.nc` is a land/sea mask provided by NASA that is used in conjunction with the global IMERG data to filter out precipitation retrievals over land.
- `README.md` provides a high-level overview of what this project is about and what each file does. You're reading it right now.

## Acknowledgement
I would like to thank Dr. Hannah Zanowski and Cameron Bertossa for teaching a wonderful overview of computational methods that will undoubtedly prove helpful in my research and my career wherever I may end up.
